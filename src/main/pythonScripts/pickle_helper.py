import pickle as pk


def read_pickle(path_to_read):
    try:
        with open(path_to_read, 'rb') as file:
            result = pk.load(file)
        return result
    except:
        return None


def write_pickle(entity, path_to_write):
    try:
        with open(path_to_write, 'wb') as file:
            result = pk.dump(entity, file)
        return True
    except:
        return False
