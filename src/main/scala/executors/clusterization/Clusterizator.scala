package executors.clusterization

import java.io.File

import akka.http.scaladsl.model.HttpResponse
import client.models.category.CategoryJSONParams
import client.models.job.JobSTATE._
import client.models.job.{JobJSON, JobJSONParams}
import client.{CategoriesClient, ImagesClient, JobsClient, SetsClient}
import executors.Executor
import util.{CSVLister, PythonStarter, S3BucketController}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.{BufferedSource, Source}
import scala.util.Success

class Clusterizator(job: JobJSON)
                   (implicit ec: ExecutionContext,
                    jobsClient: JobsClient,
                    setsClient: SetsClient,
                    imagesClient: ImagesClient,
                    categoriesClient: CategoriesClient,
                    bucketBase: S3BucketController) extends Executor {

  val pathToResource = "resources/temp/"
  val pathToVect = "resources/temp/vectors/"
  val pathToScript = "src/main/pythonScripts/clust_wrapper.py"

  val emptyVect = ""

  new File(pathToResource).mkdir

  val tempFolders: Array[String] = new File(pathToResource).listFiles().filter(_.isDirectory).map(_.getPath)
  if (!tempFolders.isEmpty) {
    throw new RuntimeException("Temp directory isn't empty!")
//        println("NonEmptyTemp")
  } else {
    val imagesVect = new File(pathToVect)
    imagesVect.mkdir
  }

  var imagesPairs: Map[String, Long] = _
  var downloadTimeout: Duration = 5 minutes
  var patchTimeout: Duration = 1 minute

  def download(verbose: Boolean = false): Future[Option[List[(String, Long)]]] = {

    setsClient.getSet(job.setId).map { set =>
      set.imagesIds
    }.flatMap { list =>
      imagesClient.getImages(list)
    }.map { imgs =>

      val go: Boolean = imgs.map(_.vector).forall(!_.equals(emptyVect))
      if (go) {
        Some(imgs.map(img => img.vector -> img.id))
      } else {
        /// if empty create a job to fill empty vectors ???
        throw new RuntimeException("Some photo don't have proper vector!")
        None
      }
    }.flatMap {
      case Some(vectors) =>
        Future.traverse(vectors) { pair =>
          if (verbose) println(s"Downloading vector $pair")
          bucketBase.downloadAndParseToFile(pathToResource, pair._1)
        }.map(_ => Some(vectors))
      case None => Future.successful(None)
    }.andThen {
      case Success(vectors) =>
        if (verbose) println(s"Succeeded: ${vectors.get.length}")
        imagesPairs = vectors.get.map {
          pair => pair._1.split('/').last.split('.')(0) -> pair._2
        }.toMap
    }
  }

  override def process(verbose: Boolean = false): Unit = {
    val listOfModels: List[String] = new File(pathToVect).listFiles.toList.map{ file =>
      val src: BufferedSource = Source.fromFile(file)
      val out: String = src.getLines.toList.last
      src.close()
      out
    }

    if (!listOfModels.forall(_ == listOfModels.head))
      throw new RuntimeException("Vect Models are not equal")

    if (verbose) println("Making CSV for python clusterizer...")

    val pathToCSV = pathToResource + "vectors.csv"
    CSVLister.makeCSV(pathToVect, pathToCSV)

    val kClusters = job.k
    val csvPath: String = new File(pathToCSV).getAbsolutePath
    val scriptPath: String = new File(pathToScript).getAbsolutePath

    if (verbose) println("Starting python clusterizer...")

    val flags: String = s" --k_clust $kClusters --vectors $csvPath"

    val script = new PythonStarter(scriptPath, flags)

    val res = script.executeScript

    if (verbose) if (res) {
      println("Script worked fine.")
    } else {
      println("During script exec error occurred")
    }
  }

  override def patch(verbose: Boolean): Future[HttpResponse] = {
    val vectors: List[String] = new File(pathToVect).listFiles().toList.map(_.getAbsolutePath)

    if (verbose) println(s"Vectors: ${vectors.length}")

    val keyPaths: List[String] = vectors.map(_.split('/').last.split('.')(0))
    val imagesIds: List[Long] = keyPaths.map(imagesPairs(_))
    val clusters: List[Int] = vectors.map { path =>
      val src: BufferedSource = Source.fromFile(path)
      val cluster: Int = src.getLines.next.toInt
      src.close()
      cluster
    }

    val categories: List[CategoryJSONParams] = clusters.zip(imagesIds).groupBy(pair => pair._1).
      mapValues(_.map(_._2)).
      map { pair =>
        CategoryJSONParams(name = Some(s"Generated: ${System.currentTimeMillis()} #: ${pair._1}"),
          imagesIds = Some(pair._2), userId = Some(job.userId))
      }.toList

    if (verbose) println(s"Categories: ${vectors.length}")

    categoriesClient.makeCategories(categories).andThen {
      case Success(_) => if (verbose) println(s"Posted new categories")
    }.flatMap { _ =>
      jobsClient.changeJob(JobJSONParams(state = Some(FINISHED)), job.id)
    }.andThen {
      case Success(_) => if (verbose) println(s"Patched job")
    }

  }

  override def fullCycle(verbose: Boolean = false): Unit = {
    Await.ready(this.download(verbose), downloadTimeout)
    this.process(verbose)
    Await.ready(this.patch(verbose), patchTimeout)
    this.finish()
  }

  override def fullCycleAsync(verbose: Boolean = false): Future[Unit] =
    this.download(verbose).flatMap { _ =>
      this.processAsync(verbose)
    }.flatMap { _ =>
      this.patch(verbose)
    }.flatMap { _ =>
      this.finishAsync
    }


  def setTimeouts(downloadTimeout: Duration = 5 minutes, patchTimeout: Duration = 1 minutes): Unit = {
    this.downloadTimeout = downloadTimeout
    this.patchTimeout = patchTimeout
  }

}
