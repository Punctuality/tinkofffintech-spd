import akka.actor.ActorSystem
import akka.http.scaladsl.model.headers.Authorization
import akka.stream.ActorMaterializer
import client._
import client.models.job.JobSTATE._
import client.models.job.{JobJSON, JobJSONParams}
import executors.Executor
import executors.clusterization.Clusterizator
import executors.collages.CollageMaker
import executors.duplicates.DuplicatesChecker
import executors.vectorization.Vectorizer
import util.S3BucketController

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext}

class JobExecutor(jobsClient: JobsClient,
                  setsClient: SetsClient,
                  imagesClient: ImagesClient,
                  categoriesClient: CategoriesClient,
                  duplicatesClient: DuplicatesClient,
                  timeout: Duration,
                  patchingTimeout: Duration,
                  downloadingTimeout: Duration,
                  verbose: Boolean)
                 (implicit
                  sys: ActorSystem,
                  mat: ActorMaterializer,
                  ec: ExecutionContext,
                  auth: Authorization,
                  s3Base: S3BucketController) extends Runnable {


  implicit val jc: JobsClient = jobsClient
  implicit val sc: SetsClient = setsClient
  implicit val ic: ImagesClient = imagesClient
  implicit val cc: CategoriesClient = categoriesClient
  implicit val dc: DuplicatesClient = duplicatesClient

  def findExecutor: PartialFunction[JobJSON, Option[Executor]] = {
    case job@JobJSON(_, _, _, _, state, _, _) => state match {
      case s if s == (FINISHED: Int) =>
        println("Jobs FINISHED")
        None
      case s if s == (FAILED: Int) =>
        println("Jobs FAILED")
        None
      case s if s == (PROCESSING: Int) =>
        println("Jobs PROCESSING")
        None
      case s if s == (VECTORIZATION: Int) =>
        println("Starting Vectorizer")
        Some(new Vectorizer(job))
      case s if s == (CLUSTERIZATION: Int) =>
        println("Starting Clustarizator")
        Some(new Clusterizator(job))
      case s if s == (DUPLICATESCHK: Int) =>
        println("Starting Duplicates checker")
        Some(new DuplicatesChecker(job))
      case s if s == (COLLAGEMAKING: Int) =>
        println("Starting Collage Maker")
        Some(new CollageMaker(job))
    }
    case _ =>
      println("Unknown pattern")
      None
  }


  override def run(): Unit = {
    val job: Option[(JobJSON, Long)] = Await.result(jobsClient.checkJobs, patchingTimeout)
    job match {
      case None =>
        println(s"No Jobs, waiting ${timeout}")
        Thread.sleep(timeout.toMillis)
      case Some((job, jobId)) =>
        Await.ready(jobsClient.changeJob(JobJSONParams(state = Some(PROCESSING)),jobId), patchingTimeout)
        findExecutor(job) match {
          case None =>
            println("No satisfied executor.")
          case Some(executor) =>
            executor.setTimeouts(downloadingTimeout, patchingTimeout)
            executor.fullCycle(true)
        }
    }
  }
}
