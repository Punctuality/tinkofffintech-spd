package client

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Authorization
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import client.models.category._
import org.json4s.jackson.Serialization._

import scala.concurrent.{ExecutionContext, Future}


class CategoriesClient(val ipAddress: String, val port: Int)
                      (implicit system: ActorSystem,
                       materializer: ActorMaterializer,
                       ec: ExecutionContext,
                       auth: Authorization) extends Json4sSupport {



  def request(request: HttpRequest): Future[HttpResponse] = {
    Http().singleRequest(request)
  }

  def getCategory(id: Long): Future[CategoryJSON] =
    request(HttpRequest(
      method = HttpMethods.GET,
      uri =  Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/categories/$id"),
      headers = List(auth)
    )).flatMap(Unmarshal(_).to[CategoryJSON])

  def changeCategory(category: CategoryJSONParams, id: Long): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.PATCH,
      uri =  Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/categories/$id"),
      entity = HttpEntity(ContentTypes.`application/json`, write(category)),
      headers = List(auth)
    ))

  def changeCategories(categories: List[(CategoryJSONParams, Long)]): Future[Done] = {
    val requests: List[HttpRequest] = categories.map(cat =>
          HttpRequest(method = HttpMethods.PATCH,
            uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/categories/${cat._2}"),
            entity = HttpEntity(ContentTypes.`application/json`, write(cat._1)),
            headers = List(auth)
          )
        )
    Source(requests).
      via(Http().outgoingConnection(ipAddress, port)).runForeach(response => response.discardEntityBytes())
  }

  def makeCategory(category: CategoryJSONParams): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.POST,
      uri =  Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/categories"),
      entity = HttpEntity(ContentTypes.`application/json`, write(category)),
      headers = List(auth)
    ))

  def makeCategories(categories: List[CategoryJSONParams]): Future[Done] = {
    val requests: List[HttpRequest] = categories.map(cat =>
      HttpRequest(method = HttpMethods.POST,
        uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/categories"),
        entity = HttpEntity(ContentTypes.`application/json`, write(cat)),
        headers = List(auth)
      )
    )

    Source(requests).
      via(Http().outgoingConnection(ipAddress, port)).runForeach(response => response.discardEntityBytes())
  }
}