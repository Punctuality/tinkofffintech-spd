import argparse

from vectorizers import *

parser = argparse.ArgumentParser(description='Choose model and provide files paths.')

parser.add_argument("--model", choices=["Standard", "VGG", "VGG_BN", "ResNet", "Squeeze"])
parser.add_argument("--batch_size", type=int)
parser.add_argument("--photos", type=str)
parser.add_argument("--vectors", type=str)

args = parser.parse_args().__dict__

print(args)


def choose_vectorisier(args):
    p_csv = args["photos"]
    b_s = args["batch_size"]

    if args["model"] == "Standard":
        return AutoEncoderVectorizer(path_to_csv=p_csv, batch_size=b_s)
    elif args["model"] == "VGG":
        return VGGVectorizer(path_to_csv=p_csv, batch_size=b_s, batch_norm=False)
    elif args["model"] == "VGG_BN":
        return VGGVectorizer(path_to_csv=p_csv, batch_size=b_s, batch_norm=True)
    elif args["model"] == "ResNet":
        return ResNetVectorizer(path_to_csv=p_csv, batch_size=b_s)
    elif args["model"] == "Squeeze":
        return SqueezeVectorizer(path_to_csv=p_csv, batch_size=b_s)


vectorizer = choose_vectorisier(args)
vectorizer.map_data()
vectorizer.save_mapping(args["vectors"])
