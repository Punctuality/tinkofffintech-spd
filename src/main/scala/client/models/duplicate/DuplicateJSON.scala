package client.models.duplicate

case class DuplicateJSON (id: Long,
                           userId: Long,
                           name: String,
                           description: String,
                           imagesIds: List[Long])