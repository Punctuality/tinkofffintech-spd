package client.models.set

case class SetJSONForList(id: Long,
                          userId: Long,
                          name: String,
                          description: String)
