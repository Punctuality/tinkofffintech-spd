import numpy as np
import pandas as pd
import torch
import torchvision
from torchvision.models import vgg16_bn, vgg16, resnet50, squeezenet1_1
from tqdm import tqdm

from dataset_loader import CustomDatasetFromImages
from photo_autoencoder import PhotoAutoEncoder


class Vectorizer:
    def map_data(self):
        pass

    def save_mapping(self, path_to_save):
        pass


class SqueezeVectorizer(Vectorizer):

    def __init__(self, path_to_csv, batch_size=16):
        """
        :type path_to_csv: str
        :type batch_size: int
        :type batch_norm: bool
        """
        self.batch_size = batch_size
        self.data_info = pd.read_csv(path_to_csv, header=None)
        self.data = CustomDatasetFromImages(path_to_csv)
        self.data_loader = torch.utils.data.DataLoader(self.data,
                                                       batch_size=self.batch_size,
                                                       shuffle=False)

        self.model = squeezenet1_1(pretrained=True)
        self.model.eval()

        self.mapping = None

    def map_data(self):
        out = torch.Tensor()

        for data, meta in tqdm(self.data_loader):
            bottleneck_repres = self.model(data).data
            out = torch.cat((out, bottleneck_repres))

        self.mapping = out.detach().numpy()

        return self.mapping

    def save_mapping(self, path_to_save):
        paths = [path.split("/")[-1].split(".")[0] for path in np.asarray(self.data_info.iloc[:, 1])[1:]]

        # print(self.mapping.shape, len(paths))

        # print("SaveMappingLog: ", paths)

        for i, path in enumerate(paths):
            with open(path_to_save+"/"+path+".vect", "w") as temp:
                temp.write(str(list(self.mapping[i])))
                temp.write("\n" + "SQUEEZE")
                temp.close()


class VGGVectorizer(Vectorizer):

    def __init__(self, path_to_csv, batch_size=16, batch_norm=True):
        """
        :type path_to_csv: str
        :type batch_size: int
        :type batch_norm: bool
        """

        self.data_info = pd.read_csv(path_to_csv, header=None)
        self.batch_size = batch_size
        self.batch_norm = batch_norm
        self.data = CustomDatasetFromImages(path_to_csv)
        self.data_loader = torch.utils.data.DataLoader(self.data,
                                                       batch_size=self.batch_size,
                                                       shuffle=False)

        self.model = vgg16_bn(pretrained=True) if (self.batch_norm) else vgg16(pretrained=True)
        self.model.eval()

        self.mapping = None

    def map_data(self):
        out = torch.Tensor()

        for data, meta in tqdm(self.data_loader):
            bottleneck_repres = self.model(data).data
            out = torch.cat((out, bottleneck_repres))

        self.mapping = out.detach().numpy()

        return self.mapping

    def save_mapping(self, path_to_save):
        paths = [path.split("/")[-1].split(".")[0] for path in np.asarray(self.data_info.iloc[:, 1])[1:]]

        # print(self.mapping.shape, len(paths))

        # print("SaveMappingLog: ", paths)

        for i, path in enumerate(paths):
            with open(path_to_save+"/"+path+".vect", "w") as temp:
                temp.write(str(list(self.mapping[i])))
                temp.write("\n" + "VGG"+("_BN" if self.batch_norm else ""))
                temp.close()


class ResNetVectorizer(Vectorizer):

    def __init__(self, path_to_csv, batch_size=16):
        """
        :type path_to_csv: str
        :type batch_size: int
        """
        self.data_info = pd.read_csv(path_to_csv, header=None)
        self.batch_size = batch_size
        self.data = CustomDatasetFromImages(path_to_csv)
        self.data_loader = torch.utils.data.DataLoader(self.data,
                                                       batch_size=self.batch_size,
                                                       shuffle=False)

        self.model = resnet50(pretrained=True)
        self.model.eval()

        self.mapping = None

    def map_data(self):
        out = torch.Tensor()

        for data, meta in tqdm(self.data_loader):
            bottleneck_repres = self.model(data).data
            out = torch.cat((out, bottleneck_repres))

        self.mapping = out.detach().numpy()

        return self.mapping

    def save_mapping(self, path_to_save):
        paths = [path.split("/")[-1].split(".")[0] for path in np.asarray(self.data_info.iloc[:, 1])[1:]]

        # print(self.mapping.shape, len(paths))

        # print("SaveMappingLog: ", paths)

        for i, path in enumerate(paths):
            with open(path_to_save+"/"+path+".vect", "w") as temp:
                temp.write(str(list(self.mapping[i])))
                temp.write("\n" + "RESNET")
                temp.close()


class AutoEncoderVectorizer(Vectorizer):

    def __init__(self, path_to_csv, path_to_weights=None, batch_size=16):
        """
        :type path_to_csv: str
        :type path_to_weights: str
        :type batch_size: int
        """
        self.batch_size = batch_size
        self.path_to_weights = path_to_weights

        self.data_info = pd.read_csv(path_to_csv, header=None)

        if path_to_weights == None:
            self.path_to_weights = "resources/weights/ae_weights.h5"

        self.data = CustomDatasetFromImages(path_to_csv,
                                            transforms=[
                                                torchvision.transforms.Resize((140, 140)),
                                                torchvision.transforms.ToTensor()])

        self.data_loader = torch.utils.data.DataLoader(self.data,
                                                       batch_size=self.batch_size,
                                                       shuffle=False)

        self.model = PhotoAutoEncoder(input_channels=3)
        self.model.load_weights(self.path_to_weights)
        self.model.eval()

        self.mapping = None

    def map_data(self):
        self.mapping = self.model.map_data(self.data_loader).numpy()

        return self.mapping

    def save_mapping(self, path_to_save):
        paths = [path.split("/")[-1].split(".")[0] for path in np.asarray(self.data_info.iloc[:, 1])[1:]]

        # print(self.mapping.shape, len(paths))

        # print("SaveMappingLog: ", paths)

        for i, path in enumerate(paths):
            with open(path_to_save+"/"+path+".vect", "w") as temp:
                temp.write(str(list(self.mapping[i])))
                temp.write("\n" + "AUTOENCODER")
                temp.close()