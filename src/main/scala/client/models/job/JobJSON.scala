package client.models.job

case class JobJSON(id: Long,
                   userId: Long,
                   setId: Long,
                   //createDT: DateTime,
                   //updateDT: DateTime,
                   name: String,
                   state: Int,
                   //priority: Int = 3,
                   k: Int,
                   model: Int
                   //path: String,
                   //retryCount: Int,
                   //tryTime: DateTime
                  )

