package client

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Authorization
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import client.models.job.JobSTATE._
import client.models.job._
import org.json4s.jackson.Serialization._

import scala.concurrent.{ExecutionContext, Future}

class JobsClient(val ipAddress: String, val port: Int)
                (implicit system: ActorSystem,
                 materializer: ActorMaterializer,
                 ec: ExecutionContext,
                 auth: Authorization) extends Json4sSupport {


  def request(request: HttpRequest): Future[HttpResponse] = {
    Http().singleRequest(request)
  }

  def getJobs: Future[Seq[JobJSON]] =
    request(HttpRequest(
      method = HttpMethods.GET,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/jobs"),
      headers = List(auth)
    )).flatMap(Unmarshal(_).to[Seq[JobJSON]])

  def getJob(id: Long): Future[JobJSON] =
    request(HttpRequest(
      method = HttpMethods.GET,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/jobs/$id"),
      headers = List(auth)
    )).flatMap(Unmarshal(_).to[JobJSON])

  def checkJobs(triggerState: JobSTATE.Value): Future[Option[(JobJSON, Long)]] =
    getJobs.map(_.find(_.state == (triggerState: Int)).map(job => job -> job.id))

  def checkJobs: Future[Option[(JobJSON, Long)]] =
    getJobs.map(_.find{job =>
      List(FINISHED, FAILED, PROCESSING)
        .forall(state => job.state != (state: Int))}.map(job => job -> job.id))

  def changeJob(job: JobJSONParams, id: Long): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.PATCH,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/jobs/$id"),
      entity = HttpEntity(ContentTypes.`application/json`, write(job)),
      headers = List(auth)
    ))

  def makeJob(job: JobJSONParams): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.POST,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/jobs"),
      entity = HttpEntity(ContentTypes.`application/json`, write(job)),
      headers = List(auth)
    ))

  def deleteJob(id: Long): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.DELETE,
      uri =  Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/jobs/$id"),
      headers = List(auth)
    ))

  def deleteJobs(ids: List[Long]): Future[Done] = {
    val requests: List[HttpRequest] = ids.map(id =>
      HttpRequest(method = HttpMethods.DELETE,
        uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/jobs/$id"),
        headers = List(auth)
      )
    )
    Source(requests).
      via(Http().outgoingConnection(ipAddress, port)).runForeach(response => response.discardEntityBytes())
  }
}