package client.models.duplicate

case class DuplicateJSONParams(userId: Option[Long] = None,
                                name: Option[String] = None,
                                description: Option[String] = None,
                                imagesIds: Option[List[Long]] = None)
