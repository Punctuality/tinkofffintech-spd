package client.models.job

case class JobJSONParams(name: Option[String] = None,
                         k: Option[Int] = None,
                         model: Option[Int] = None,
                         state: Option[Int] = None,
                         //priority: Option[Int] = None,
                         setId: Option[Long] = None)
