package client.models.image

case class ImageJSON(id: Long,
                     userId: Long,
                     name: String,
                     description: String,
                     url: String,
                     vector: String)
