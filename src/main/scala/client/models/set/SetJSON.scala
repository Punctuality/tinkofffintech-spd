package client.models.set

case class SetJSON(id: Long,
                   userId: Long,
                   name: String,
                   description: String,
                   imagesIds: List[Long])
