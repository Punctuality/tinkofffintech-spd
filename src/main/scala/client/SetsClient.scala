package client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Authorization
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import client.models.set._
import org.json4s.jackson.Serialization._

import scala.concurrent.{ExecutionContext, Future}

class SetsClient(val ipAddress: String, val port: Int)
                (implicit system: ActorSystem,
                 materializer: ActorMaterializer,
                 ec: ExecutionContext,
                 auth: Authorization) extends Json4sSupport {


  def request(request: HttpRequest): Future[HttpResponse] = {
    Http().singleRequest(request)
  }

  def getSet(id: Long): Future[SetJSON] = {
    request(HttpRequest(
      method = HttpMethods.GET,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/sets/$id"),
      headers = List(auth)
    )).flatMap(Unmarshal(_).to[SetJSON])
  }

  def makeSet(set: SetJSONParams): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.POST,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/sets"),
      entity = HttpEntity(ContentTypes.`application/json`, write(set)),
      headers = List(auth)
    ))
}