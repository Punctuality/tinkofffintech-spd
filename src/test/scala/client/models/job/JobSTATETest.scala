package client.models.job

import client.models.job.JobSTATE._
import org.scalatest.{FlatSpec, Matchers}

class JobSTATETest extends FlatSpec with Matchers{

  behavior of "int conversion"

  it should "give proper ints" in {
    val eqInt: (Int, Int) => Boolean = (a, b) => a == b

    eqInt(PROCESSING,0) shouldBe  true
    eqInt(VECTORIZATION,1) shouldBe  true
    eqInt(CLUSTERIZATION,2) shouldBe  true
    eqInt(DUPLICATESCHK,3) shouldBe  true
    eqInt(COLLAGEMAKING,4) shouldBe  true
    eqInt(FINISHED,5) shouldBe true
    eqInt(FAILED,6) shouldBe  true
  }

}
