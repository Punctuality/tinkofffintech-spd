package util

import awscala.File
import awscala.s3.{Bucket, S3, S3Object, S3ObjectSummary}
import com.amazonaws.services.s3.model.{ObjectMetadata, S3ObjectInputStream}
import com.amazonaws.services.{s3 => aws}

import scala.concurrent.{ExecutionContext, Future}

/** Class for controlling Amazon S3 Bucket Storage.
  * Include async implementation of base methods and automatic bucket setup.
  * Or in other words, reliable API for [[awscala.s3.Bucket]]
  *
  * @constructor By given name choose whether to get existing bucket or to create new one
  * @param bucketName Bucket name
  * @param s3         Amazon S3 scala client
  * @param ec         Future ExecutionContext
  */
class S3BucketController(bucketName: String)(implicit val s3: S3, ec: ExecutionContext) {
  private[this] val curBucket: Bucket = s3.bucket(bucketName).getOrElse(s3.createBucket(bucketName))
  private[this] var curObj: Option[S3Object] = None

  /** Sets a new current `S3Object` for `curObj` methods
    * @param obj Object to set
    */
  @deprecated
  def setCurObj(obj: S3Object): Unit =
    curObj = Some(obj)

  /** Gets a current `S3Object` for `curObj` methods
    * @return Option value of `S3Object`
    */
  @deprecated
  def getCurObj: Option[S3Object] =
    curObj

  /** `curObj` variant of [[util.S3BucketController#uploadFile(java.lang.String, java.lang.String)]]
    *
    * @return Future value of uploaded object
    */
  @deprecated
  def uploadFile: Future[S3Object] = curObj match {
    case Some(obj) => Future {
      curBucket.putObject(obj.key, obj.content, obj.metadata)
    }.flatMap(_ => downloadFile(obj.key))
    case None => Future.failed(new RuntimeException("No current Object!"))
  }

  /** Uploads given file to Amazon S3 servers
    * Does set a new object for `curObj` methods
    *
    * @param name     Key name for Amazon S3
    * @param filePath Path to the file which to upload
    * @return Future value [[Unit]]
    */
  def uploadFile(name: String, filePath: String): Future[Unit] =
    Future {
      curBucket.put(name, new File(filePath))
    }

  /** Uploads given file to Amazon S3 servers
    * Does set a new object for `curObj` methods
    * Not Async
    *
    * @param name     Key name for Amazon S3
    * @param filePath Path to the file which to upload
    * @return [[Unit]]
    */
  def uploadFileBlocking(name: String, filePath: String): Unit = {
    curBucket.put(name, new File(filePath))
  }

  /** Downloads file from Amazon S3 servers by given key
    * Does set a new object for `curObj` methods
    *
    * @param name Key name for Amazon S3
    * @return Future value of downloaded object
    */
  def downloadFile(name: String): Future[S3Object] =
    Future {
      curBucket.get(name) match {
        case Some(obj) =>
          setCurObj(obj)
          obj
        case None => throw new aws.model.AmazonS3Exception("Couldn't download file!")
      }
    }

  /** `curObj` variant of [[util.S3BucketController#deleteFile(java.lang.String)]]
    *
    * @return Future value of deleted object
    */
  @deprecated
  def deleteFile: Future[S3Object] = curObj match {
    case Some(obj) => deleteFile(obj.key)
    case None => Future.failed(new RuntimeException("No current Object!"))
  }

  /** Deletes file from Amazon S3 servers by given key
    * Does set a new object for `curObj` methods
    *
    * @param name Key name for Amazon S3
    * @return Future value of deleted object
    */
  def deleteFile(name: String): Future[S3Object] =
    downloadFile(name).map { obj =>
      obj.destroy
      obj
    }

  /** `curObj` variant of [[util.S3BucketController#fileInfo(java.lang.String)]]
    *
    * @return Future value of object's metadata
    */
  @deprecated
  def fileInfo: Future[ObjectMetadata] = curObj match {
    case Some(obj) => fileInfo(obj.key)
    case None => Future.failed(new RuntimeException("No current Object!"))
  }

  /** Downloading Metadata of file from Amazon S3 servers by given key
    * Does set a new object for `curObj` methods
    *
    * @param name Key name for Amazon S3
    * @return Future value of object's metadata
    */
  def fileInfo(name: String): Future[ObjectMetadata] =
    Future {
      curBucket.getMetadata(name)
    }

  /** Gets Bucket's size in bytes
    *
    * @return Bucket's size in bytes
    */
  def bucketSize: Future[Long] =
    Future {
      curBucket.totalSize
    }

  /** Returns sequence of object summaries for current bucket and prefix
    *
    * @param prefix Prefix for files to return
    * @return Future value of Stream with Info: Left(prefixes), Right(objects)
    */
  def bucketLS(prefix: String): Future[Stream[Either[String, S3ObjectSummary]]] = Future {
    curBucket.ls(prefix)
  }

  /** Returns sequence of object keys for current bucket and prefix
    *
    * @param prefix Prefix for files to return
    * @return Future value of Stream of [[java.lang.String]]
    */
  def bucketFiles(prefix: String): Future[Stream[String]] =
    bucketLS(prefix).map(_.filter(_.isRight).map({
      case Right(value) => value.getKey
    }))

  /** Returns sequence of folders for current bucket and prefix
    *
    * @param prefix Prefix for folders to return
    * @return Future value of Stream of [[java.lang.String]]
    */
  def bucketFolders(prefix: String): Future[Stream[String]] =
    bucketLS(prefix).map(_.filter(_.isLeft).map({
      case Left(value) => value
    }))

  /** Returns sequence of folders and files for current bucket and prefix
    *
    * @param prefix Prefix for folders to return
    * @return Future value of Stream of [[java.lang.String]]
    */
  def bucketStructure(prefix: String): Future[Stream[String]] =
    Future(curBucket.keys().toStream)

  /** `curObj` variant of [[util.S3BucketController#parseToFile(java.lang.String, scala.concurrent.Future)]]
    *
    * @return Future value of Unit
    */
  @deprecated
  def parseToFile(folderPath: String): Future[Unit] = curObj match {
    case Some(obj) => parseToFile(folderPath, obj)
    case None => Future.failed(new RuntimeException("No current Object!"))
  }

  /** Writing S3Object to file
    *
    * @param folderPath Directory path where to write file
    * @param obj        Object which to write
    * @return Future value of Unit
    */
  def parseToFile(folderPath: String, obj: S3Object): Future[Unit] = Future {
    parseToFileBlocking(folderPath, obj)
  }

  /** Analog of [[util.S3BucketController#parseToFile(java.lang.String, scala.concurrent.Future)]]
    *
    * @param folderPath Directory path where to write file
    * @param obj        Future value of S3Object which to write
    * @return Future value of Unit
    */
  def parseToFile(folderPath: String, obj: Future[S3Object]): Future[Unit] =
    obj.flatMap(parseToFile(folderPath, _))


  /** Downloads file from Amazon S3 servers by given key and instantly saves it
    * Does set a new object for `curObj` methods
    *
    * @param name Key name for Amazon S3
    * @return Future value of downloaded object
    */
  def downloadAndParseToFile(folderPath: String, name: String): Future[S3Object] = Future{
    curBucket.get(name) match {
      case Some(obj) =>
        parseToFileBlocking(folderPath, obj)
        obj
      case None => throw new aws.model.AmazonS3Exception("Couldn't download file!")
    }
  }

  /** Downloads file from Amazon S3 servers by given key and instantly saves it
    * Does set a new object for `curObj` methods
    * Not Async
    * @param name Key name for Amazon S3
    * @return Value of downloaded object
    */
  def downloadAndParseToFileBlocking(folderPath: String, name: String): S3Object = {
    curBucket.get(name) match {
      case Some(obj) =>
        println(obj.key)
        parseToFileBlocking(folderPath, obj)
        obj
      case None => throw new aws.model.AmazonS3Exception("Couldn't download file!")
    }
  }

  /** Writing S3Object to file
    * Not Async
    * @param folderPath Directory path where to write file
    * @param obj        Object which to write
    * @return Value of Unit
    */
  def parseToFileBlocking(folderPath: String, obj: S3Object): Unit = {
    import java.io.FileOutputStream

    val path: String = folderPath + obj.key
    val in: S3ObjectInputStream = obj.getObjectContent
    val buf = new Array[Byte](1024)
    val file = new File(path)
    val out = new FileOutputStream(file)

    var go: Boolean = true
    while (go) {
      val count: Int = in.read(buf)
      if (count != -1) {
        if (Thread.interrupted) throw new InterruptedException
        out.write(buf, 0, count)
      } else {
        go = false
      }
    }
    out.flush()
    out.close()

    println(s"Finished parsing: ${obj.key}")
  }
}
