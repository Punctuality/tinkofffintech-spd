package executors.vectorization

object Model extends Enumeration{
  val Standard, VGG, VGG_BN, ResNet, Squeeze = Value

  implicit def modelToInt: PartialFunction[Model.Value, Int] = {
    case Standard => 1;
    case VGG => 2;
    case VGG_BN => 3;
    case ResNet => 4;
    case Squeeze => 5
  }

  implicit def intToModel: PartialFunction[Int, Model.Value] = {
    case 1 => Standard;
    case 2 => VGG;
    case 3 => VGG_BN;
    case 4 => ResNet;
    case 5 => Squeeze
  }
}
