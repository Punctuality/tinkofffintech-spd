package client.models.category

case class CategoryJSONForList(id: Long,
                                userId: Long,
                                name: String,
                                description: String)
