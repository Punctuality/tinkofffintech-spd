import numpy as np
import pandas as pd
import torch
import torchvision
from PIL import Image


class CustomDatasetFromImages(torch.utils.data.Dataset):

    def __init__(self, csv_path, transforms=None):
        """
        Args:
            csv_path (string): path to csv file
            transforms: pytorch transforms for transforms and tensor conversion
        """
        # Transforms
        if transforms is None:
            transforms = [torchvision.transforms.Resize((224, 224)), torchvision.transforms.ToTensor()]

        self.trans = torchvision.transforms.Compose(transforms)
        # Read the csv file
        self.data_info = pd.read_csv(csv_path, header=None)
        # First column contains the image paths
        self.ind_arr = np.asarray(self.data_info.iloc[:, 0])[1:]
        # Second column is the labels
        self.path_arr = np.asarray(self.data_info.iloc[:, 1])[1:]
        # Calculate len
        self.data_len = len(self.data_info.index) - 1

    def __getitem__(self, index):
        # Get image name from the pandas df
        single_image_name = self.path_arr[index]
        # Open image
        img_as_img = Image.open(single_image_name)

        # Transform image to tensor
        img_as_tensor = self.trans(img_as_img)

        # Get label(class) of the image based on the cropped pandas column
        single_image_label = self.ind_arr[index]

        return img_as_tensor, single_image_label

    def __len__(self):
        return self.data_len

