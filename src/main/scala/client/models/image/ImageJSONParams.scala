package client.models.image

case class ImageJSONParams(name: Option[String] = None,
                           description: Option[String] = None,
                           url: Option[String] = None,
                           vector: Option[String] = None)
