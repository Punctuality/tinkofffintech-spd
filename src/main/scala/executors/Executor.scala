package executors

import java.io.File

import akka.http.scaladsl.model.HttpResponse

import scala.concurrent.duration.Duration
import scala.concurrent.{ExecutionContext, Future}

abstract class Executor(implicit val ec: ExecutionContext) {

  val pathToResource: String

  def finish(): Unit = {
    val tempDir = new File(pathToResource)

    def deleteRecursively(file: File): Unit = {
      if (file.isDirectory)
        file.listFiles.foreach(deleteRecursively)
      if (file.exists && !file.delete)
        throw new Exception(s"Unable to delete ${file.getAbsolutePath}")
    }

    deleteRecursively(tempDir)
  }

  def finishAsync: Future[Unit] = Future {
    this.finish()
  }

  def download(verbose: Boolean): Future[_]

  def process(verbose: Boolean): Unit

  def processAsync(verbose: Boolean): Future[Unit] = Future(process(verbose))

  def patch(verbose: Boolean): Future[HttpResponse]

  def fullCycle(verbose: Boolean): Unit

  def fullCycleAsync(verbose: Boolean): Future[Unit]

  def setTimeouts(downloadTimeout: Duration, patchTimeout: Duration): Unit

}
