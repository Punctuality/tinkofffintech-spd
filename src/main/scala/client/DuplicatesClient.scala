package client

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Authorization
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import client.models.duplicate._
import org.json4s.jackson.Serialization._

import scala.concurrent.{ExecutionContext, Future}


class DuplicatesClient(val ipAddress: String, val port: Int)
                      (implicit system: ActorSystem,
                       materializer: ActorMaterializer,
                       ec: ExecutionContext,
                       auth: Authorization) extends Json4sSupport {



  def request(request: HttpRequest): Future[HttpResponse] = {
    Http().singleRequest(request)
  }

  def getDuplicate(id: Long): Future[DuplicateJSON] =
    request(HttpRequest(
      method = HttpMethods.GET,
      uri =  Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/duplicates/$id"),
      headers = List(auth)
    )).flatMap(Unmarshal(_).to[DuplicateJSON])

  def changeDuplicate(duplicate: DuplicateJSONParams, id: Long): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.PATCH,
      uri =  Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/duplicates/$id"),
      entity = HttpEntity(ContentTypes.`application/json`, write(duplicate)),
      headers = List(auth)
    ))

  def changeDuplicates(duplicates: List[(DuplicateJSONParams, Long)]): Future[Done] = {
    val requests: List[HttpRequest] = duplicates.map(cat =>
      HttpRequest(method = HttpMethods.PATCH,
        uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/duplicates/${cat._2}"),
        entity = HttpEntity(ContentTypes.`application/json`, write(cat._1)),
        headers = List(auth)
      )
    )
    Source(requests).
      via(Http().outgoingConnection(ipAddress, port)).runForeach(response => response.discardEntityBytes())
  }

  def makeDuplicate(duplicate: DuplicateJSONParams): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.POST,
      uri =  Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/duplicates"),
      entity = HttpEntity(ContentTypes.`application/json`, write(duplicate)),
      headers = List(auth)
    ))

  def makeDuplicates(duplicates: List[DuplicateJSONParams]): Future[Done] = {
    val requests: List[HttpRequest] = duplicates.map(cat =>
      HttpRequest(method = HttpMethods.POST,
        uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/duplicates"),
        entity = HttpEntity(ContentTypes.`application/json`, write(cat)),
        headers = List(auth)
      )
    )
    Source(requests).
      via(Http().outgoingConnection(ipAddress, port)).runForeach(response => response.discardEntityBytes())
  }
}