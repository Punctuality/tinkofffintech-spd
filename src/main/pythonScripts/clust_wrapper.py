import argparse
import ast
import numpy as np
import pandas as pd

from kmeans_clusterizer import *

# from src.main.pythonScripts.kmeans_clusterizer import *

parser = argparse.ArgumentParser(description='Choose K_cluster and provide files paths.')

parser.add_argument("--k_clust", type=int)
parser.add_argument("--vectors", type=str)

args = parser.parse_args().__dict__

print(args)

clusterizer = KMeansClusterizer(args["k_clust"])

data_info = pd.read_csv(args["vectors"], header=None)
path_arr = list(np.asarray(data_info.iloc[:, 1])[1:])

def extract_vector(path_to_vector):
    with open(path_to_vector, 'r') as file:
        vector = ast.literal_eval(file.readline())
        return vector


def put_cluster(path_to_file, cluster_id):
    with open(path_to_file, 'w') as file:
        file.write(str(cluster_id))


vectors = np.array([extract_vector(path) for path in path_arr])

print("Vectors shape: {} {}".format(vectors.shape, vectors.dtype))

clusterizer.mapping = vectors
score = clusterizer.fit()

print("Clusterization score: {}".format(score))

_, ids = clusterizer.transform()

print(ids)

[put_cluster(path, id) for (path, id) in zip(path_arr, list(ids))]