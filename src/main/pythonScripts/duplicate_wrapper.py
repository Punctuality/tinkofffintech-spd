import argparse

from duplicates import *

# from src.main.pythonScripts.duplicates import *

parser = argparse.ArgumentParser(description='Provide files paths.')

parser.add_argument("--photos", type=str)
parser.add_argument("--result", type=str)

args = parser.parse_args().__dict__

print(args)

checker = Duplicates(args["photos"])

print("Duplicates found: {}".format(len(checker.filtered_pairs)))
print("Hashes {}".format(list(zip(checker.paths, checker.hashes))))

checker.save_duplicates(args["result"])

print("Saved duplicates")
