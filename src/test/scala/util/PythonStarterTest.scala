package util

import org.scalatest.{FlatSpec, Matchers}

class PythonStarterTest extends FlatSpec with Matchers {

  val goodScript = new PythonStarter("src/test/resources/pythonGoodTest.py", "--secs 5")
  val badScript = new PythonStarter("src/test/resources/pythonBadTest.py")

  behavior of "executeScript"

  it should "work properly and in blocking way" in {
    goodScript.withFlags = false
    val startTime: Long = System.currentTimeMillis

    val res: Boolean = goodScript.executeScript
    res shouldBe true

    val endTime: Long = System.currentTimeMillis()
    (endTime - startTime > 10000) shouldBe true
  }

  it should "work properly with Flags" in {
    goodScript.withFlags = true
    val startTime: Long = System.currentTimeMillis

    val res: Boolean = goodScript.executeScript
    res shouldBe true

    val endTime: Long = System.currentTimeMillis()
    val diff: Long = endTime - startTime
    (diff < 10000) && (diff > 5000) shouldBe true
  }

  it should "return False if program executed not in normal way" in {
    val res: Boolean = badScript.executeScript
    res shouldBe false
  }

}
