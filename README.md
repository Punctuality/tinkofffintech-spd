# TinkoffFintech-SPD


## About 

The idea of the project is to implement photo processing **gallery/storage** in [**Scala** language](https://www.scala-lang.org) (Almost) with such features as: 

* auto-clusterisation
* finding similar photos (duplicates)
* making collages from chosen photos
* *at least with ability to upload and download images* 
* Registration service and private galleries (*Additional feature*)
* Integration as a bot service (for example - Vk bot, Telegram bot) (*Additional feature*)

## Structure

Project is splitted in two independent parts:

1. First program responsible for **UI**, controling storage and making new jobs for second program.
2. Second program is **distributable** and responsible for processing **ML** and other **time intensive computations**.

[Project WIKI](https://gitlab.com/Punctuality/tinkofffintech-spd/wikis/home)

Project is made as a graduation project for [**Tinkoff Fintech** - Scala track - 2019](https://fintech.tinkoff.ru/tfschool/scala)