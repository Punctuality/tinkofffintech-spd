package executors.vectorization


import java.io.File
import java.util.concurrent.TimeoutException

import akka.http.scaladsl.model.HttpResponse
import client.models.image.{ImageJSON, ImageJSONParams}
import client.models.job.JobSTATE._
import client.models.job._
import client.{ImagesClient, JobsClient, SetsClient}
import executors.Executor
import executors.vectorization.Model._
import util.{CSVLister, PythonStarter, S3BucketController}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Success


class Vectorizer(val job: JobJSON, val batchSize: Int = 10, val replaceVector: Boolean = false)
                (implicit ec: ExecutionContext,
                 setsClient: SetsClient,
                 jobsClient: JobsClient,
                 imagesClient: ImagesClient,
                 bucketBase: S3BucketController) extends Executor {
  val pathToResource = "resources/temp/"
  val pathToVect = "resources/temp/vectors/"
  val pathToImages = "resources/temp/images/"
  val pathToScript = "src/main/pythonScripts/vect_wrapper.py"

  val emptyVect = ""

  new File(pathToResource).mkdir

  val tempFolders: Array[String] = new File(pathToResource).listFiles().filter(_.isDirectory).map(_.getPath)
  if (!tempFolders.isEmpty) {
    throw new RuntimeException("Temp directory isn't empty!")
    //    println("NonEmptyTemp")
  } else {
    val imagesVect = new File(pathToVect)
    val imagesDir = new File(pathToImages)
    imagesDir.mkdir()
    imagesVect.mkdir()
  }

  var imagesPairs: Map[String, Long] = _
  var downloadTimeout: Duration = 5 minutes
  var patchTimeout: Duration = 5 minutes

  override def download(verbose: Boolean = false): Future[List[(Long, String)]] = {

    setsClient.getSet(job.setId).map { set =>
      set.imagesIds
    }.flatMap { list =>
      imagesClient.getImages(list)
    }.map { imgs =>
      val fimgs: List[ImageJSON] = if (!replaceVector) imgs.filter(_.vector.equals(emptyVect)) else imgs
      fimgs.map { img =>
        img.id -> img.url
      }
    }.flatMap { list =>
      Future.traverse(list) { pair =>
        if (verbose) println(s"Downloading $pair")
        bucketBase.downloadAndParseToFile(pathToResource, pair._2)
      }.map(_ => list)
    }.andThen { case Success(list) =>
      if (verbose) println(s"Succeeded: ${list.length}")
      imagesPairs = list.map(_.swap).map {
        pair => pair._1.split('/').last.split('.')(0) -> pair._2
      }.toMap
    }
  }

  override def process(verbose: Boolean = false): Unit = {

    if (verbose) println("Making CSV for python vectorizer...")

    val pathToCSV = pathToResource + "images.csv"
    CSVLister.makeCSV(pathToImages, pathToCSV)

    val modelInt: Int = job.model

    val model: Model.Value = intToModel(modelInt)
    val csvPath: String = new File(pathToCSV).getAbsolutePath
    val vectorsPath: String = new File(pathToVect).getAbsolutePath
    val scriptPath: String = new File(pathToScript).getAbsolutePath

    if (verbose) println("Starting python vectorizer...")

    val flags: String = s" --model $model --batch_size $batchSize --photos $csvPath --vectors $vectorsPath"

    val script = new PythonStarter(scriptPath, flags)

    val res = script.executeScript

    if (verbose) if (res) {
      println("Script worked fine.")
    } else {
      println("During script exec error occurred")
    }
  }

  override def patch(verbose: Boolean = false): Future[HttpResponse] = {
    val vectors: List[String] = new File(pathToVect).listFiles().toList.map(_.getAbsolutePath)

    if (verbose) println(s"Vectors: ${vectors.length}")

    val keyPaths: List[String] = vectors.map(_.split('/').last.split('.')(0))
    val imagesIds: List[Long] = keyPaths.map(imagesPairs(_))
    val s3Paths: List[String] = keyPaths.map(path => s"vectors/$path.vect")
    val imageParams: List[ImageJSONParams] = s3Paths.map(path => ImageJSONParams(vector = Some(path)))

    s3Paths.indices.foreach { index =>
      val s3Path = s3Paths(index)
      val vectorPath = vectors(index)
      bucketBase.uploadFileBlocking(s3Path, vectorPath)
      if (verbose) println(s"Uploded vector (#$index): $s3Path")
    }

    imagesClient.changeImages(imageParams.zip(imagesIds)).andThen {
      case Success(_) => if (verbose) println(s"Patched images vectors")
    }.flatMap { _ =>
      jobsClient.changeJob(JobJSONParams(state = Some(FINISHED)), job.id)
    }.andThen {
      case Success(_) => if (verbose) println(s"Patched job")
    }
  }

  override def fullCycle(verbose: Boolean = false): Unit = {
    Await.ready(this.download(verbose), downloadTimeout)
    this.process(verbose)
    Await.ready(this.patch(verbose), patchTimeout).recoverWith{
      case _: TimeoutException =>
        setTimeouts(downloadTimeout, patchTimeout * 5)
        this.patch(verbose)
    }
    this.finish()
  }

  override def fullCycleAsync(verbose: Boolean = false): Future[Unit] =
    this.download(verbose)
      .flatMap { _ =>
        this.processAsync(verbose)
      }.flatMap { _ =>
      this.patch(verbose)
    }.flatMap { _ =>
      this.finishAsync
    }

  def setTimeouts(downloadTimeout: Duration = 5 minutes, patchTimeout: Duration = 5 minutes): Unit = {
    this.downloadTimeout = downloadTimeout
    this.patchTimeout = patchTimeout
  }

}
