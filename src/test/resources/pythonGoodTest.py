import argparse
from time import sleep

parser = argparse.ArgumentParser(description='Test parsing')

parser.add_argument("--secs", type=int)

args = parser.parse_args().__dict__

def sleep_and_wait(x):
    for i in range(x):
        sleep(1)
        print(i)

standart = lambda x: 10 if x == None else x

sleep_and_wait(standart(args["secs"]))