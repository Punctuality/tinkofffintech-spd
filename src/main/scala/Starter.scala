import akka.actor.ActorSystem
import akka.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials}
import akka.stream.ActorMaterializer
import awscala.Region
import awscala.s3.S3
import client._
import util.S3BucketController

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}

object Starter {
  implicit val sys: ActorSystem = ActorSystem()
  implicit val mat: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global

  implicit val auth = Authorization(BasicHttpCredentials("admin", "admin"))
  implicit val s3 = S3.at(Region.EU_WEST_3)
  implicit val bucket = new S3BucketController("tinkoff-fintech-spd")

  def main(args: Array[String]): Unit = {
    val ip: String = if (args.isEmpty) "35.228.220.177" else args(0)
    val port: Int = if (args.isEmpty) 8000 else args(1).toInt

    val jc = new JobsClient(ip, port)
    val sc = new SetsClient(ip, port)
    val ic = new ImagesClient(ip, port)
    val cc = new CategoriesClient(ip, port)
    val dc = new DuplicatesClient(ip, port)

    val executor: JobExecutor = new JobExecutor(
      jc,
      sc,
      ic,
      cc,
      dc,
      1 minute,
      1 minute,
      5 minutes,
      true
    )

    println("Started looping")

    while (true) {
      println("Searching for job...")
      executor.run()
      println("Finished job!")
    }
  }
}
