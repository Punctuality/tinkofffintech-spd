name := "SPD-processing-unit"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % "10.1.8",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.8" % Test,
  "com.typesafe.akka" %% "akka-testkit" % "2.5.22" % Test,
  "com.typesafe.akka" %% "akka-stream" % "2.5.22",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.22" % Test,
  "com.github.tototoshi" %% "scala-csv" % "1.3.5",
  "com.github.seratch" %% "awscala-ec2" % "0.8.+",
  "com.github.seratch" %% "awscala-iam" % "0.8.+",
  "com.github.seratch" %% "awscala-dynamodb" % "0.8.+",
  "com.github.seratch" %% "awscala-emr" % "0.8.+",
  "com.github.seratch" %% "awscala-redshift" % "0.8.+",
  "com.github.seratch" %% "awscala-s3" % "0.8.+",
  "com.github.seratch" %% "awscala-simpledb" % "0.8.+",
  "com.github.seratch" %% "awscala-sqs" % "0.8.+",
  "com.github.seratch" %% "awscala-sts" % "0.8.+",
  "com.github.seratch" %% "awscala-stepfunctions" % "0.8.+",
  "org.json4s" %% "json4s-jackson" % "3.6.5",
  "org.scalatest" %% "scalatest" % "3.0.3" % Test
)