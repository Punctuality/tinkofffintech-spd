package util

import java.io.File

import com.github.tototoshi.csv.CSVWriter

object CSVLister{

  def makeCSV(pathToFiles: String, pathToCSV: String): Unit = {
    val writer: CSVWriter = CSVWriter.open(pathToCSV)

    val listOfFiles =
      new File(new File(pathToFiles).getAbsolutePath).
        listFiles.map(_.getAbsolutePath).toList

    println(s"Length of list: ${listOfFiles.length}")

    val toWrite: Seq[Seq[String]] = Seq("index","path") +: listOfFiles.indices.map(ind =>
      Seq(ind.toString, listOfFiles(ind)))

    writer.writeAll(toWrite)
    writer.close()
  }
}
