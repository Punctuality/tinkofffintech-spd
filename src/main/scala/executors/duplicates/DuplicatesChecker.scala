package executors.duplicates

import java.io.File

import akka.http.scaladsl.model.HttpResponse
import client._
import client.models.duplicate.DuplicateJSONParams
import client.models.job.JobSTATE._
import client.models.job.{JobJSON, JobJSONParams}
import executors.Executor
import util._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.{BufferedSource, Source}
import scala.util.Success

class DuplicatesChecker(val job: JobJSON)
                       (implicit ec: ExecutionContext,
                        setsClient: SetsClient,
                        jobsClient: JobsClient,
                        imagesClient: ImagesClient,
                        duplicatesClient: DuplicatesClient,
                        bucketBase: S3BucketController) extends Executor {
  val pathToResource = "resources/temp/"
  val pathToImages = "resources/temp/images/"
  val pathToScript = "src/main/pythonScripts/duplicate_wrapper.py"
  val pathToResult = "resources/temp/result.data"

  new File(pathToResource).mkdir

  val tempFolders: Array[String] = new File(pathToResource).listFiles().filter(_.isDirectory).map(_.getPath)
  if (!tempFolders.isEmpty) {
    throw new RuntimeException("Temp directory isn't empty!")
    //    println("NonEmptyTemp")
  } else {
    val imagesDir = new File(pathToImages)
    imagesDir.mkdir()
  }

  var imagesPairs: Map[String, Long] = _
  var downloadTimeout: Duration = 5 minutes
  var patchTimeout: Duration = 5 minutes

  override def download(verbose: Boolean = false): Future[List[(Long, String)]] = {
    setsClient.getSet(job.setId).map { set =>
      set.imagesIds
    }.flatMap { list =>
      imagesClient.getImages(list)
    }.map { imgs =>
      imgs.map { img =>
        img.id -> img.url
      }
    }.flatMap { list =>
      Future.traverse(list) { pair =>
        if (verbose) println(s"Downloading $pair")
        bucketBase.downloadAndParseToFile(pathToResource, pair._2)
      }.map(_ => list)
    }.andThen { case Success(list) =>
      if (verbose) println(s"Succeeded: ${list.length}")
      imagesPairs = list.map(_.swap).map {
        pair => pair._1.split('/').last.split('.')(0) -> pair._2
      }.toMap
    }
  }

  override def process(verbose: Boolean): Unit = {

    if (verbose) println("Making CSV for python duplicates checker...")

    val pathToCSV = pathToResource + "images.csv"
    CSVLister.makeCSV(pathToImages, pathToCSV)

    val csvPath: String = new File(pathToCSV).getAbsolutePath
    val scriptPath: String = new File(pathToScript).getAbsolutePath
    val resultPath: String = new File(pathToResult).getAbsolutePath

    if (verbose) println("Starting python duplicates checker...")

    val flags: String = s" --photos $csvPath --result $resultPath"

    val script = new PythonStarter(scriptPath, flags)

    val res = script.executeScript

    if (verbose) if (res) {
      println("Script worked fine.")
    } else {
      println("During script exec error occurred")
    }
  }

  override def patch(verbose: Boolean): Future[HttpResponse] = {
    val src: BufferedSource = Source.fromFile(pathToResult)
    val duplicatesPath: List[List[String]] = src.getLines.toList.map(_.split(";").toList)
    val duplicates: List[DuplicateJSONParams] = duplicatesPath.map { paths =>
      DuplicateJSONParams(userId = Some(job.userId), imagesIds = Some(
        paths.map { path =>
          imagesPairs(path.split('/').last.split('.')(0))
        }
      )
      )
    }

    if (verbose) println(s"Duplicates: ${duplicates.length}")

    duplicatesClient.makeDuplicates(duplicates).andThen {
      case Success(_) => if (verbose) println(s"Posted new duplicates")
    }.flatMap { _ =>
      jobsClient.changeJob(JobJSONParams(state = Some(FINISHED)), job.id)
    }.andThen {
      case Success(_) => if (verbose) println(s"Patched job")
    }
  }

  override def fullCycle(verbose: Boolean = false): Unit = {
    Await.ready(this.download(verbose), downloadTimeout)
    this.process(verbose)
    Await.ready(this.patch(verbose), patchTimeout)
    this.finish()
  }

  override def fullCycleAsync(verbose: Boolean = false): Future[Unit] =
    this.download(verbose)
      .flatMap {
        _ =>
          this.processAsync(verbose)
      }.flatMap {
      _ =>
        this.patch(verbose)
    }.flatMap {
      _ =>
        this.finishAsync
    }


  def setTimeouts(downloadTimeout: Duration = 5 minutes, patchTimeout: Duration = 1 minutes): Unit = {
    this.downloadTimeout = downloadTimeout
    this.patchTimeout = patchTimeout
  }

}
