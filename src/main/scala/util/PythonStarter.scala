package util

import scala.concurrent.{ExecutionContext, Future}
import sys.process._

/** Util for starting python script from Scala environment
  * @param pathToScript Path to file of python script
  * @param flags Additional flags for python script to parse
  */
class PythonStarter(pathToScript: String, flags: String = ""){
  private[this] val executionPrefix = "python3"

  /** Indicates whether to use flags or not
    */
  var withFlags = true

  /** Executes python script in blocking way.
    * @return `true` if program executed normally, `false` if not
    */
  def executeScript: Boolean = (List(executionPrefix,pathToScript,if(withFlags) flags else "").mkString(" ") !) == 0

  /** Executes python script in async way.
    * @return [[scala.concurrent.Future]] boolean value: `true` if program executed normally, `false` if not
    */
  def executeScriptAsync(implicit ec: ExecutionContext): Future[Boolean] = Future(executeScript)
}
