package client.models.set

case class SetJSONParams(name: Option[String] = None,
                         description: Option[String] = None,
                         imagesIds: Option[List[Long]] = None)
