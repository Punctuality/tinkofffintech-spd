package client.models.category

case class CategoryJSONParams(name: Option[String] = None,
                              description: Option[String] = None,
                              imagesIds: Option[List[Long]] = None,
                              userId: Option[Long] = None)
