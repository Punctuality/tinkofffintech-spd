package client.models.category

case class CategoryJSON(id: Long,
                        userId: Long,
                        name: String,
                        description: String,
                        imagesIds: List[Long])
