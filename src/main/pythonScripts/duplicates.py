import numpy as np
import pandas as pd
from PIL import Image, ImageStat
from itertools import groupby
from tqdm import tqdm


class Duplicates:
    def __init__(self, path_to_csv):
        self.path_to_csv = path_to_csv
        self.paths = self.load_paths()
        self.hashes = [self.hash_image(path) for path in tqdm(self.paths)]
        self.pairs = list(
            [(key, [elem [-1] for elem in group]) for key, group in groupby(sorted(zip(self.hashes,
                                                                                self.paths)),
                                                                            lambda x: x[0])]
        )
        self.filtered_pairs = list(filter(lambda pair: len(pair[-1]) > 1, self.pairs))
        
    def save_duplicates(self, path_to_save):
        dups_paths = [i[-1] for i in self.filtered_pairs]
        with open(path_to_save, "w") as file:
            for dup_group in dups_paths:
                line = self.mk_string(dup_group)
                file.write(line+"\n")

    @staticmethod
    def mk_string(line, sep =";"):
        out = ""
        for elem in line:
            out += elem + sep
        return out[:-len(sep)]

    def load_paths(self):
        data_info = pd.read_csv(self.path_to_csv, header=None)
        path_arr = np.asarray(data_info.iloc[:, 1])[1:]
        return path_arr
    
    @staticmethod
    def hash_image(image_path):
        img = Image.open(image_path).resize((8,8), Image.LANCZOS).convert(mode="L")
        mean = ImageStat.Stat(img).mean[0]
        return sum((1 if p > mean else 0) << i for i, p in enumerate(img.getdata()))