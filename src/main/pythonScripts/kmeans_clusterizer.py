from sklearn.cluster import KMeans
from sklearn.decomposition import PCA

from pickle_helper import read_pickle, write_pickle


class KMeansClusterizer:
    def __init__(self, n_cluster = 10):
        self.n_clusters = n_cluster

        self.mapping = None
        self.reduced_mapping = None

        self.kmeans_pca = PCA(n_components=512)
        self.kmeans = KMeans(self.n_clusters)

    def load_mapping(self, path_to_mapping):
        self.mapping = read_pickle(path_to_mapping)

    def setup_clusterizer(self, path_to_load):
        self.kmeans_pca = read_pickle(path_to_load + "/kmeans_pca.pkl")
        self.kmeans = read_pickle(path_to_load + "/kmeans.pkl")

    def save_clusterizer(self, path_to_save):
        write_pickle(self.kmeans_pca, path_to_save + "/kmeans_pca.pkl")
        write_pickle(self.kmeans, path_to_save + "/kmeans.pkl")

    def transform(self):
        pca_compr = self.kmeans_pca.transform(self.mapping)
        cluster_id = self.kmeans.predict(pca_compr)
        return pca_compr, cluster_id

    def fit(self):
        pca_compr = self.kmeans_pca.fit_transform(self.mapping)
        cluster_ids = self.kmeans.fit_predict(pca_compr)
        return self.kmeans.score(pca_compr, cluster_ids)
