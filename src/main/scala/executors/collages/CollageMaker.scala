package executors.collages

import java.io.File
import java.util.UUID

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Authorization
import client._
import client.models.job.JobSTATE._
import client.models.job.{JobJSON, JobJSONParams}
import executors.Executor
import util._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Success

class CollageMaker(val job: JobJSON,
                   val width: Int = 800,
                   val initHeight: Int = 300)
                  (implicit ec: ExecutionContext,
                   setsClient: SetsClient,
                   jobsClient: JobsClient,
                   imagesClient: ImagesClient,
                   auth: Authorization,
                   bucketBase: S3BucketController) extends Executor {

  val collageName: String = UUID.randomUUID().toString + ".jpg"

  val pathToResource = "resources/temp/"
  val pathToImages = "resources/temp/images/"
  val pathToScript = "src/main/pythonScripts/collage_maker.py"
  val pathToResult = s"resources/temp/$collageName"

  new File(pathToResource).mkdir

  val tempFolders: Array[String] = new File(pathToResource).listFiles().filter(_.isDirectory).map(_.getPath)
  if (!tempFolders.isEmpty) {
    throw new RuntimeException("Temp directory isn't empty!")
    //    println("NonEmptyTemp")
  } else {
    val imagesDir = new File(pathToImages)
    imagesDir.mkdir()
  }

  var downloadTimeout: Duration = 5 minutes
  var patchTimeout: Duration = 5 minutes

  override def download(verbose: Boolean = false): Future[List[(Long, String)]] = {
    setsClient.getSet(job.setId).map { set =>
      set.imagesIds
    }.flatMap { list =>
      imagesClient.getImages(list)
    }.map { imgs =>
      imgs.map { img =>
        img.id -> img.url
      }
    }.flatMap { list =>
      Future.traverse(list) { pair =>
        if (verbose) println(s"Downloading $pair")
        bucketBase.downloadAndParseToFile(pathToResource, pair._2)
      }.map(_ => list)
    }
  }

  override def process(verbose: Boolean): Unit = {

    val srcPath: String = new File(pathToImages).getAbsolutePath
    val scriptPath: String = new File(pathToScript).getAbsolutePath
    val resultPath: String = new File(pathToResult).getAbsolutePath

    if (verbose) println("Starting python collage maker...")

    val flags: String = s" -f $srcPath -o $resultPath -s -w $width -i $initHeight"

    val script = new PythonStarter(scriptPath, flags)

    val res = script.executeScript

    if (verbose) if (res) {
      println("Script worked fine.")
    } else {
      println("During script exec error occurred")
    }
  }

  override def patch(verbose: Boolean): Future[HttpResponse] = {
    val collage: File = new File(pathToResult)
    val formData: Multipart.FormData = Multipart.FormData.fromFile("image",
      ContentTypes.`application/octet-stream`,
      collage,
      100000)

    imagesClient.request(HttpRequest(
      method = HttpMethods.POST,
      uri = Uri.from(scheme = "http",
        host = imagesClient.ipAddress,
        port = imagesClient.port,
        path = s"/images/image"),
      headers = List(auth),
      entity = formData.toEntity
    )).andThen {
      case Success(repr) => if (verbose) {
        println(repr)
        println(s"Posted new collage")
      }
    }.flatMap { _ =>
      jobsClient.changeJob(JobJSONParams(state = Some(FINISHED)), job.id)
    }.andThen {
      case Success(_) => if (verbose) println(s"Patched job")
    }
  }

  override def fullCycle(verbose: Boolean = false): Unit = {
    Await.ready(this.download(verbose), downloadTimeout)
    this.process(verbose)
    Await.ready(this.patch(verbose), patchTimeout)
    this.finish()
  }

  override def fullCycleAsync(verbose: Boolean = false): Future[Unit] =
    this.download(verbose)
      .flatMap { _ =>
        this.processAsync(verbose)
      }.flatMap { _ =>
      this.patch(verbose)
    }.flatMap { _ =>
      this.finishAsync
    }


  def setTimeouts(downloadTimeout: Duration = 5 minutes, patchTimeout: Duration = 1 minutes): Unit = {
    this.downloadTimeout = downloadTimeout
    this.patchTimeout = patchTimeout
  }

}
