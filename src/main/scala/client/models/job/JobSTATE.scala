package client.models.job

object JobSTATE extends Enumeration {
  val PROCESSING, VECTORIZATION, CLUSTERIZATION, DUPLICATESCHK, COLLAGEMAKING, FINISHED, FAILED = Value

  implicit def jobStateToInt: PartialFunction[JobSTATE.Value, Int] = {
    case PROCESSING => 0;
    case VECTORIZATION => 1;
    case CLUSTERIZATION => 2;
    case DUPLICATESCHK => 3;
    case COLLAGEMAKING => 4;
    case FINISHED => 5;
    case FAILED => 6
  }
}
