import torch
from torch.nn import *
from tqdm import tqdm


class PhotoAutoEncoder(Module):
    def __init__(self, input_channels):
        super(PhotoAutoEncoder, self).__init__()

        self.input_channels = input_channels
        self.encoder_layers = [
            # fst conv
            Conv2d(1, 16, (3, 3)),
            ReLU(),
            MaxPool2d(kernel_size=(2,2)),
            # snd conv
            Conv2d(16, 2, (3, 3)),
            ReLU(),
            MaxPool2d(kernel_size=(2,2))
        ]
        self.decoder_layers = [
            # fst deconv
            ConvTranspose2d(2, 2, (3, 3)),
            ReLU(),
            UpsamplingNearest2d(scale_factor=2),
            # snd deconv
            ConvTranspose2d(2, 16, (2, 2)),
            ReLU(),
            UpsamplingNearest2d(scale_factor=2),
            # some weird stuff to match dimensions
            Conv2d(16, 1,(3, 3)),
            Sigmoid()
        ]
        self.encoder = Sequential(*self.encoder_layers)
        self.decoder = Sequential(*self.decoder_layers)

        self.train_setuped = False

    def __encoder__(self, x):
        out = torch.Tensor()
        for i in range(self.input_channels):
            temp_res = self.encoder(x[:, i, :, :][:, None, :, :])
            out = torch.cat((out, temp_res), dim = 1)

        return out

    def __decoder__(self, x):
        out = torch.Tensor()
        for i in range(0, self.input_channels * 2, 2):
            temp_res = self.decoder(x[:, i:i+2, :, :])
            out = torch.cat((out, temp_res), dim = 1)

        return out

    def calc_bottleneck(self, x):
        out = self.__encoder__(x)
        out = out.resize(out.size()[0], out.size()[1]*out.size()[2]*out.size()[3]).detach()
        return out

    def map_data(self, image_loader):
        out = torch.Tensor()

        for data, meta in tqdm(image_loader):
            bottleneck_repres = self.calc_bottleneck(data).data
            out = torch.cat((out, bottleneck_repres))

        return out.detach()

    def forward(self, x):
        out = self.__encoder__(x)
        out = self.__decoder__(out)
        return out

    # def save_weights(self, path):
    #     torch.save(obj = self.state_dict(), f = path)

    def load_weights(self, path):
        self.load_state_dict(torch.load(path))
        self.eval()