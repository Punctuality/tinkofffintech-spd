package client

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Authorization
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import client.models.image._
import org.json4s.jackson.Serialization._

import scala.concurrent.{ExecutionContext, Future}

class ImagesClient(val ipAddress: String, val port: Int)
                  (implicit system: ActorSystem,
                   materializer: ActorMaterializer,
                   ec: ExecutionContext,
                   auth: Authorization) extends Json4sSupport {


  def request(request: HttpRequest): Future[HttpResponse] = {
    Http().singleRequest(request)
  }

  def getImages: Future[Seq[ImageJSON]] =
    request(HttpRequest(
      method = HttpMethods.GET,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/images"),
      headers = List(auth)
    )).flatMap(Unmarshal(_).to[Seq[ImageJSON]])

  def getImage(id: Long): Future[ImageJSON] =
    request(HttpRequest(
      method = HttpMethods.GET,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/images/$id"),
      headers = List(auth)
    )).flatMap(Unmarshal(_).to[ImageJSON])

  def getImages(ids: List[Long]): Future[List[ImageJSON]] = {
    val requests: List[HttpRequest] = ids.map(id =>
      HttpRequest(
        method = HttpMethods.GET,
        uri = Uri(s"/images/$id"),
        headers = List(auth)
      )
    )

    Source(requests).
      via(Http().outgoingConnection(ipAddress, port)).
      runFoldAsync(List().asInstanceOf[List[ImageJSON]]){(list, response) =>
        Unmarshal(response).to[ImageJSON].map(res => res +: list)
      }
  }

  def changeImage(img: ImageJSONParams, id: Long): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.PATCH,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/images/$id"),
      entity = HttpEntity(ContentTypes.`application/json`, write(img)),
      headers = List(auth)
    ))

  def changeImages(imgs: List[(ImageJSONParams, Long)]): Future[Done] = {
    val requests: List[HttpRequest] = imgs.map(img =>
      HttpRequest(method = HttpMethods.PATCH,
        uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/images/${img._2}"),
        entity = HttpEntity(ContentTypes.`application/json`, write(img._1)),
        headers = List(auth)
      )
    )

    Source(requests).
      via(Http().outgoingConnection(ipAddress, port)).runForeach(response => response.discardEntityBytes())
  }

  def makeImage(img: ImageJSONParams): Future[HttpResponse] =
    request(HttpRequest(
      method = HttpMethods.POST,
      uri = Uri.from(scheme = "http", host = ipAddress, port = port, path = s"/images"),
      entity = HttpEntity(ContentTypes.`application/json`, write(img)),
      headers = List(auth)
    ))
}